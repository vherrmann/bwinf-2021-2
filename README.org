#+TITLE: 2. Runde des Bundeswettbewerb Informatik 2021

Diese Arbeit hat mir einen 1. Preis in der 2. Runde des Bundeswettbewerb Informatik 2021 eingebracht.

Die Dokumentation befindet sich in file:./bin/Aufgabe3/doc/doc.pdf und file:./bin/Aufgabe4/doc/doc.pdf. Sie kann am Einfachsten mit folgendem Befehl gebaut werden:
#+begin_src sh
$ latexmk -xelatex doc.tex
#+end_src

Um die Programme zu verwenden, empfehle ich Nix. Mit Nix genügt der Befehl ~nix develop~ um eine funktionierende Entwicklungsumgebung zu bekommen. In dieser Umgebung können die Programme folgendermaßen benutzt werden:
#+begin_src sh
$ cabal run $TARGET -- $ARGS
#+end_src

Um das Programm für Aufgabe4 mit zufällig generierten Lochkarten zu benutzen, kann also der Befehl
#+begin_src sh
$ cabal run aufgabe4 -- zufall 128 11 111
#+end_src
verwendet werden. Die unterstützten Befehle/Flags sind in der Dokumentation und im Quellcode ausführlich beschrieben.
