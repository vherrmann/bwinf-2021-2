module Main (main) where

import Control.Lens (view, _3)
import Control.Monad (guard, replicateM)
import Data.Bifunctor (bimap, first, second)
import Data.Bits
import Data.Bool (bool)
import Data.Char
import Data.Foldable (Foldable (toList), asum, foldl', maximumBy)
import Data.Function (on, (&))
import Data.Functor (void, (<&>))
import Data.Maybe (fromJust, fromMaybe)
import Data.Traversable (mapAccumR)
import Data.Vector (Vector)
import qualified Data.Vector as V
import Data.Void (Void)
import Data.Word (Word8)
import GHC.Exts (fromList)
import Options.Applicative (execParser)
import qualified Options.Applicative as O
import System.Exit (die)
import System.Random (randomRIO)
import Text.Megaparsec
import qualified Text.Megaparsec.Char as C
import qualified Text.Megaparsec.Char.Lexer as L

--- Hilfsmittel

-- | Erstellt einen String mit den Ziffern der Zahl `n` in Binärdarstellung
-- bis Stelle `l`.
showBin :: Bits a => a -> Int -> String
showBin n l =
  reverse
    [ bool '0' '1' (testBit n i)
      | i <- [0 .. l - 1]
    ]

--- Typen

-- | `HexMax` stellt ein Rätsel dar. Dabei ist `umleg` die Maximalanzahl an Umlegungen.
-- Ist `umleg` gleich `Nothing`, so ist keine Maximalanzahl gegeben.
data HexMax = HexMax {hexZahl :: [Int], umleg :: Maybe Int}
  deriving stock (Show, Read)

-- | `Segment` stellt mit seinen 7 ersten Bits eine Ziffer einer Siebensegmentanzeige
-- dar. Das letzte Bit ist stets 0.
newtype Segment = Segment Word8
  deriving newtype (Show, Num, Bits, Eq, Enum, FiniteBits)

-- | `Konf` stellt die Befehlszeilenkonfikuration dar.
data Konf = Konf {noLog :: Bool, varLänge :: Bool, minHexZahl :: Bool, untBefehle :: UntBefehle}
  deriving stock (Show)

-- | `Unterbefehle` stellt die Unterbefehle des Programms dar.
data UntBefehle = UBDirekt HexMax | UBDatei String Bool | UBZufall Int (Maybe Int)
  deriving stock (Show)

--- Lösung

-- | In diesem Vektor sind die Segmente jeweils an dem Index, den sie auch
-- in der Siebensegmentanzeige darstellen.
segmentVektor :: Vector Segment
segmentVektor =
  fromList
    [ 0b1110111, -- 0
      0b0010010, -- 1
      0b1011101, -- …
      0b1011011,
      0b0111010,
      0b1101011,
      0b1101111,
      0b1010010,
      0b1111111,
      0b1111011,
      0b1111110,
      0b0101111,
      0b1100101,
      0b0011111,
      0b1101101,
      0b1101100
    ]

-- | In diesem Vektor sind die Zahlen jeweils an dem Index dessen Wert ihre Präsentation
-- in der Siebensegmentdarstellung ist.
-- Da nicht alle möglichen Segmente einer Ziffer entsprechen sind die anderen Positionen
-- leer, also mit Nothing besetzt.
zifferVektor :: Vector (Maybe Int)
zifferVektor =
  fromList $
    [ Segment i `V.elemIndex` segmentVektor
      | i <- [0 .. maxBound :: Word8]
    ]

-- | Diese Funktion konvertiert ein Segment falls möglich zur entsprechenden Zahl.
segmentZuZiffer :: Segment -> Maybe Int
segmentZuZiffer (Segment seg) = zifferVektor V.! fromIntegral seg

-- | Diese Funktion konvertiert die Zahl `x` zum entsprechenden Segment.
zifferZuSegment :: Int -> Segment
zifferZuSegment x = segmentVektor V.! x

-- | Diese Funktion bestimmt, wie viele Stäbchen beim Übergang von s₁ zu s₂
-- weggenommen und hingelegt werden müssen.
wegUndHinWechselSeg :: Segment -> Segment -> (Int, Int)
wegUndHinWechselSeg s1 s2 =
  -- `zähleEiner` bestimmt wie viele Stäbchen beim Übergang sₓ zu sᵧ wegmüssen.
  let zähleEiner sx sy = popCount $ sx \\ sy
   in (zähleEiner s1 s2, zähleEiner s2 s1)

-- | `wegUndHinWechselSeg`, nur mit Zahlen anstatt Segmenten.
wegUndHinWechsel :: Int -> Int -> (Int, Int)
wegUndHinWechsel = wegUndHinWechselSeg `on` zifferZuSegment

-- | Cache für `wegUndHinWechsel`
wegUndHinWechselV :: Vector (Vector (Int, Int))
wegUndHinWechselV =
  fromList $
    [0 .. 15] <&> \i ->
      fromList $
        [0 .. 15] <&> \j ->
          wegUndHinWechsel i j

-- | `maxDiffZwWegUndHin` bestimmt für die Ziffer `zf` zum einen wie viele
-- Stäbchen von der Ziffer maximal weggenommen werden können und wie viele
-- Stäbchen der Ziffer maximal hingelegt werden können beim Wechsel zu einer
-- anderen Ziffer.
maxDiffZwWegUndHin :: Int -> (Int, Int)
maxDiffZwWegUndHin zf =
  let ls = uncurry (-) . (wegUndHinWechselV V.! zf V.!) <$> [0 .. 15]
   in (maximum ls, abs (minimum ls))

-- | Cache für `maxDiffZwWegUndHin`
maxDiffZwWegUndHinV :: Vector (Int, Int)
maxDiffZwWegUndHinV = fromList $ maxDiffZwWegUndHin <$> [0 .. 15]

-- | `firstOne` bestimmt die größte Potenz von 2, welche Teiler `n`s ist.
-- Also gilt `firstOne 0b10 = 10` oder `firstOne 0b101000 = 1000`
-- `firstOne` bestimmt die /erste/ Eins in der Binärdarstellung von `n`.
firstOne :: (FiniteBits a, Num a) => a -> a
firstOne n =
  head do
    i <- [0 .. finiteBitSize n - 1]
    let x = bit i .&. n
    guard (x /= 0)
    pure x

-- | `\\` entfernt aus `a` alle angeschalteten Bits, welche auch in `b` an sind.
(\\) :: Bits a => a -> a -> a
a \\ b = (a `xor` b) .&. a

-- | `löseHexMax` löst ein HexMax Rästel mit den Einstellungen `minHexZahl` und `varLänge`.
-- Gibt ein Tupel mit der Anzahl der benötigten Umlegungen und der größten
-- Hexzahl zurrück.
löseHexMax :: Bool -> Bool -> HexMax -> (Int, [Int])
löseHexMax minHexZahl varLänge (HexMax{..}) =
  let umleg' = fromMaybe maxBound umleg
   in if minHexZahl
        then löseHexMin varLänge hexZahl umleg'
        else
          if varLänge
            then löseHexMaxVarLänge hexZahl umleg'
            else löseHexMaxfixeLänge hexZahl umleg'

-- | `verknüpfeLimits` fügt den Elementen in `hexZahl` jeweils zwei Limits zu.
-- Das Erste bestimmt, wie viele Stäbchen maximal vom Teil rechts des Elements
-- (das Element inklusive) weggenommen werden können.
-- Das Zweite bestimmt, wie viele Stäbchen maximal auf der Seite hingelegt werden
-- können.
verknüpfeLimits :: [Int] -> [(Int, (Int, Int))]
verknüpfeLimits hexZahl =
  snd $
    mapAccumR
      ( \acc zf ->
          let (maxWeg, maxHin) = maxDiffZwWegUndHinV V.! zf
              nAcc = bimap (maxWeg +) (maxHin +) acc
           in (nAcc, (zf, nAcc))
      )
      (0, 0)
      hexZahl

-- | Diese Funktion bearbeitet eine verallgemeinerung des Problems der Aufgabenstellung.
-- Sie gibt ein Tupel mit einem Tupel mit der Anzahl der weggelegten und hingelegten
-- Stäbchen und der größten Hexzahl zurrück.
-- Das Argument `wegSumme'` beschreibt, wie viele Stäbchen
-- von der Hexzahl weggelegt werden können. Das Argument `hinSumme'` wie viele
-- Stäbchen hingelegt werden können, wobei `löseHexMax`
löseHexMaxfixeLängeWHS :: [Int] -> Int -> Int -> Maybe ((Int, Int), [Int])
löseHexMaxfixeLängeWHS hexZahl wegSumme hinSumme =
  verknüpfeLimits hexZahl
    & go wegSumme hinSumme
 where
  -- `go` bestimmt die größte HexZahl für den Subabschnitt im 3. Argument mit
  -- `wegSumme'-x` mal Stäbchen weglegen und `hinSumme'-x` mal Stäbchen hinlegen.
  -- Dabei gilt x∈ℕ₀.
  go :: Int -> Int -> [(Int, (Int, Int))] -> Maybe ((Int, Int), [Int])
  go 0 0 = \rs -> Just ((wegSumme, hinSumme), fst <$> rs)
  go !wegSumme' !hinSumme' =
    \case
      ((zf, (maxWeg, maxHin)) : rs) ->
        let summenDiff = wegSumme' - hinSumme'
         in -- Die folgende Überprüfung ist äquivalent zu
            -- `wegSumme' > maxWeg + hinSumme' || hinSumme' > maxHin + wegSumme'`
            -- Falls einer der beiden Werte wahr ist, kann egal wie die Stäbchen von
            -- nun an noch gelegt werden, `hinSumme'` (bzw. `wegSumme'`) nicht mehr so
            -- groß werden wie `wegSumme'` (bzw. `hinsumme'`)
            if summenDiff > maxWeg || (-summenDiff) > maxHin
              then Nothing
              else
                asum $ -- Die größte Ziffer die nicht Nothing zugewiesen bekommt, wird genommen.
                  [15, 14 .. 0] <&> \i ->
                    let (wegZfZuI, hinZfZuI) = wegUndHinWechselV V.! zf V.! i
                     in if wegZfZuI > wegSumme' || hinZfZuI > hinSumme'
                          then Nothing
                          else
                            go (wegSumme' - wegZfZuI) (hinSumme' - hinZfZuI) rs
                              <&> second (i :)
      [] ->
        -- Die restlichen erlaubten Umlegungen sind gleich `wegSumme'` und `hinSumme`.
        -- Daher müssen die Werte gleich und positiv sein.
        -- `wegSumme - wegSumme'` und `hinSumme - hinSumme'` sind jeweils wie viele
        -- Stäbchen weg und hin bewegt worden sind.
        if wegSumme' == hinSumme' && wegSumme' >= 0
          then Just ((wegSumme - wegSumme', hinSumme - hinSumme'), [])
          else Nothing

-- | Diese Funktion bearbeitet das eigentliche Problem der Aufgabenstellung.
-- Sie gibt ein Tupel mit der Anzahl der benötigten Umlegungen und der größten
-- Hexzahl zurrück. Das Argument `wegSumme'` beschreibt, wie viele Stäbchen
-- von der Hexzahl weggelegt werden können. Das Argument `hinSumme'` wie viele
-- Stäbchen hingelegt werden können, wobei `löseHexMaxfixeLänge`
löseHexMaxfixeLänge :: [Int] -> Int -> (Int, [Int])
löseHexMaxfixeLänge hexZahl umleg =
  löseHexMaxfixeLängeWHS hexZahl umleg umleg
    -- Da 2. und 3. Argument zu `löseHexMaxfixeLängeWHS` beide `umleg` sind,
    -- gibt kann immer die Lösung der gleichen Zahl gefunden werden,
    -- also eine Lösung bei der kein Stäbchen umgelegt wird.
    & fromJust
    & first fst

-- | Diese Funktion findet die größte HexZahl so wie in der Aufgabenstellung festgelegt,
-- nur dass die Ziffernzahl nicht gleich bleiben muss.
-- Sie gibt ein Tupel mit der Anzahl der benötigten Umlegungen und der größten
-- Hexzahl zurrück.
löseHexMaxVarLänge :: [Int] -> Int -> (Int, [Int])
löseHexMaxVarLänge hexZahl umleg =
  let (maxWegWegZüge, maxWegHinZüge) = maxWegZüge hexZahl
      überbleibMaxWegZüge = maxWegWegZüge - maxWegHinZüge
      -- Diese Funktion erstellt den Vorderteil der Lösung. Dabei gilt, dass mehr Zahlen
      -- besser sind. Da die Eins mit 2 Stäbchen am Wenigsten braucht, wird sie hier
      -- immer benutzt, außer die Anzahl der verfügbaren Stäbchen ist ungerade,
      -- dann muss eine Sieben benutzt werden. Diese sollte offensichtlich an erster
      -- Stelle stehen.
      erstelleVorderZiffern n =
        if even n
          then replicate (n `div` 2) 1
          else 7 : replicate ((n `div` 2) - 1) 1
   in if
          -- Falls `überbleibMaxWegZüge` kleinergleich 1 ist, kann keine zusätzliche Ziffer
          -- an die Zahl angefügt werden. Da diese mindestens 2 Segmente brauchen.
          | überbleibMaxWegZüge <= 1 -> löseHexMaxfixeLänge hexZahl umleg
          -- Falls `maxWegWegZüge` kleinergleich `umleg` ist, gibt es mindestens eine
          -- Lösung mit dieser Anzahl an Umlegungen.
          | maxWegWegZüge <= umleg ->
            let (_, lösungFix) =
                  fromJust $
                    löseHexMaxfixeLängeWHS hexZahl maxWegWegZüge maxWegHinZüge
             in (maxWegWegZüge, erstelleVorderZiffern überbleibMaxWegZüge <> lösungFix)
          -- Wir wissen, dass von `hexZahl` maximal `überbleibMaxWegZüge` mehr
          -- Stäbchen weg als hingelegt werden können.
          -- Da `maxWegWegZüge` größer als `umleg` ist, verwenden wir `umleg` für
          -- die Anzahl der weggelegten Stäbchen. Für die Anzahl der hingelegten
          -- Stäbchen benutzen wir `max² 0 (umleg - überbleibMaxWegZüge¹)` bis `umleg -2³`.
          -- Zu 1: Wir wissen, dass bei `hexZahl` maximal `überbleibMaxWegZüge`
          -- mehr Stäbchen weggenommen als hingelegt werden können.
          -- Zu 2: Eine negativer Wert für eine Anzahl ergibt keinen Sinn.
          -- Zu 3: Sind die übrigen Umlegungen <2, so kann eh keine weitere Ziffer
          -- erstellt werden. Sollte es in diesem Bereich also keine mögliche Lösung
          -- geben. So ist die größte Hexzahl mit variabler Länge äquivalent zur
          -- größten Hexzahl mit fixer Länge.
          | otherwise ->
            fromMaybe (löseHexMaxfixeLänge hexZahl umleg) do
              ((wegZüge, hinZüge), lösungFix) <-
                asum
                  ( [max 0 (umleg - überbleibMaxWegZüge) .. umleg - 2]
                      <&> \hinSumme -> löseHexMaxfixeLängeWHS hexZahl umleg hinSumme
                  )
              let überbleibWegZüge = wegZüge - hinZüge
              pure (wegZüge, erstelleVorderZiffern überbleibWegZüge <> lösungFix)
 where
  -- Diese Funktion bestimmt für die Ziffer `zf` den Wechsel zu der Ziffer,
  -- bei welcher am meisten Segmente weggewechselt werden können.
  -- Das Ergebnis ist die Anzahl der Segmente zum Wegwechseln und zum Hinwechseln.
  maxWegZügeZf :: Int -> (Int, Int)
  maxWegZügeZf zf =
    ([15, 14 .. 0] <&> \i -> wegUndHinWechselV V.! zf V.! i)
      & maximumBy (compare `on` uncurry (-))

  -- Cache für `maxWegZügeZf`
  maxWegZügeZfV :: Vector (Int, Int)
  maxWegZügeZfV = fromList $ maxWegZügeZf <$> [0 .. 15]

  -- `maxWegZüge` bestimmt, wie viele Segmente von der gegebenen Liste maximal
  -- entfernt werden können, sodass eine Sequenz von Ziffern erhalten bleibt.
  maxWegZüge :: [Int] -> (Int, Int)
  maxWegZüge =
    foldl'
      ( \acc zf ->
          let (wW, hW) = maxWegZügeZfV V.! zf
           in bimap (wW +) (hW +) acc
      )
      (0, 0)

-- | Diese Funktion findet die *kleinste* HexZahl ähnlich der Aufgabenstellung,
-- nur dass die Ziffernzahl nicht gleich bleiben muss, falls `varLänge` wahr ist.
-- Sie gibt ein Tupel mit der Anzahl der benötigten Umlegungen und der größten
-- Hexzahl zurrück.
löseHexMin :: Bool -> [Int] -> Int -> (Int, [Int])
löseHexMin varLänge hexZahl umleg =
  verknüpfeLimits hexZahl
    & go umleg umleg True
    -- Mindestens eine valide Umlegung existiert, und zwar die Umlegung zur Startzahl.
    & fromJust
 where
  -- `go` bestimmt die größte HexZahl für den Subabschnitt im 3. Argument mit
  -- `wegSumme'-x` mal Stäbchen weglegen und `hinSumme'-x` mal Stäbchen hinlegen.
  -- Dabei gilt x∈ℕ₀.
  -- Der Wahrheitswert `vorherigeStelleLeer` bestimmt, ob vor der ersten Ziffer
  -- des angegeben Abschnitts in der Lösung eine Zahl liegen wird oder nicht.
  go :: Int -> Int -> Bool -> [(Int, (Int, Int))] -> Maybe (Int, [Int])
  go 0 0 _ = \rs -> Just (umleg, fst <$> rs)
  go !wegSumme !hinSumme vorherigeStelleLeer =
    \case
      ((zf, (maxWeg, maxHin)) : rs) ->
        let summenDiff = wegSumme - hinSumme
            -- Falls die vorherige „Ziffer“ leer ist, darf diese Ziffer nicht 0 sein,
            -- da Zahlen nicht eine Null als erste Ziffer haben können.
            -- Wegen der Sortierung der Ziffern werden kleinere Ziffern bei der Erstellung
            -- der Lösung bevorzugt.
            möglicheZiffern = if vorherigeStelleLeer then [1 .. 15] else [0 .. 15]
            -- Falls die vorherige „Ziffer“ leer ist, kann diese Ziffer auch leer sein.
            möglicheLeereStelle =
              if varLänge && vorherigeStelleLeer
                then
                  let wegZfZuI = popCount $ zifferZuSegment zf
                   in if wegZfZuI > wegSumme
                        then Nothing
                        else go (wegSumme - wegZfZuI) hinSumme True rs
                else Nothing
         in -- Die folgende Überprüfung ist äquivalent zu
            -- `wegSumme' > maxWeg + hinSumme' || hinSumme' > maxHin + wegSumme'`
            -- Falls einer der beiden Werte wahr ist, kann egal wie die Stäbchen von
            -- nun an noch gelegt werden, `hinSumme'` (bzw. `wegSumme'`) nicht mehr so
            -- groß werden wie `wegSumme'` (bzw. `hinsumme'`)
            if (-summenDiff) > maxHin || summenDiff > maxWeg
              then Nothing
              else
                möglicheLeereStelle
                  <|> asum
                    ( möglicheZiffern <&> \i ->
                        let (wegZfZuI, hinZfZuI) = wegUndHinWechselV V.! zf V.! i
                         in if hinZfZuI > hinSumme || wegZfZuI > wegSumme
                              then Nothing
                              else
                                go (wegSumme - wegZfZuI) (hinSumme - hinZfZuI) False rs
                                  <&> second (i :)
                    )
      [] ->
        if wegSumme == hinSumme && wegSumme >= 0
          then Just (umleg - wegSumme, [])
          else Nothing

-- | `erstelleUmlegungen` erstellt aus der Anzahl der tatsächlichen Umlegungen,
-- der Startzahl und der Lösungszahl die Zwischenergebnisse.
erstelleUmlegungen :: Int -> [Int] -> [Int] -> [[Segment]]
erstelleUmlegungen uml hexZahl lösung =
  let diff = length lösung - length hexZahl
      -- `erstelleUmlegungen'` verarbeitet Listen an Segmenten mit gleicher Länge
      ggbSeg = replicate diff 0 <> (zifferZuSegment <$> hexZahl)
      erzSeg = replicate (-diff) 0 <> (zifferZuSegment <$> lösung)
   in -- `erstelleUmlegungen'` gibt nur sinnvolle Ergebnise aus, wenn
      -- die Startzahl länger als die Lösungszahl ist. Ist dem nicht so, so
      -- werden Start und Lösung umgedreht und die erzeugten Zwischenstände
      -- in ihrere Reihenfolge gedreht.
      if length hexZahl < length lösung
        then reverse $ erstelleUmlegungen' uml erzSeg ggbSeg
        else erstelleUmlegungen' uml ggbSeg erzSeg

-- | `erstelleUmlegungen'` erstellt aus der Anzahl der tatsächlichen Umlegungen,
-- der Startzahl und der Lösungszahl die Zwischenergebnisse. Dabei müssen
-- Startzahl und Lösungszahl gleich lang sein. Nullen können benutzt werden
-- um leere Stellen zu beschreiben. Damit die erstellten Zwischenergebnisse
-- keine Löcher haben muss die Lösungszahl weniger tatsächliche Ziffern haben
-- als die Startzahl.
erstelleUmlegungen' :: Int -> [Segment] -> [Segment] -> [[Segment]]
erstelleUmlegungen' uml hexZahl lösung =
  let (u, zwStändeInZiffer) =
        foldr
          ( \i (uml, zwStände) ->
              -- Die Zwischenstände an Stelle `i` und die Anzahl der Umlegungen
              -- wird bestimmt.
              let (nUml, nZwStände) = erzeugeZwStandInDerZiffer i (head zwStände)
               in (uml + nUml, nZwStände <> zwStände)
          )
          (0, [hexZahlV])
          [0 .. length hexZahl - 1]
          -- Die Zwischenstände sind in der falschen Reihenfolge
          & second reverse
      hexZahlV = fromList hexZahl
   in (toList <$> init zwStändeInZiffer)
        -- Iterative Erzeugung der Zwischenstände, welche über Ziffern hinweg
        -- Stäbchen wechslen
        <> ( toList . view _3
              <$> take
                (uml - u)
                (iterate erzeugeZwStand (0, 0, last zwStändeInZiffer))
           )
        <> [lösung]
 where
  lösungV = fromList lösung

  -- `erzeugeZwStandInDerZiffer` bestimmt die Zwischenschritte welche sich nur
  -- innerhalb von der Ziffer an Stelle `i` Stäbchen wechseln.
  erzeugeZwStandInDerZiffer :: Int -> Vector Segment -> (Int, [Vector Segment])
  erzeugeZwStandInDerZiffer i zwischenZahlV =
    -- `nUml` ist die Anzahl der Umlegungen, die benötigt werden
    let nUml = uncurry min $ wegUndHinWechselSeg (zwischenZahlV V.! i) (lösungV V.! i)
        -- `versionenZiff` stellt die Zwischenstände der Ziffer dar.
        versionenZiff =
          ( iterate
              ( \w ->
                  ( let h = lösungV V.! i
                     in w `xor` firstOne (h \\ w)
                          `xor` firstOne (w \\ h)
                  )
              )
              (zwischenZahlV V.! i)
              -- Der Anfangswert ist nicht benötigt
              & drop 1
              -- Die Ziffer muss `nUml`-mal verändert werden
              & take nUml
          )
     in ( nUml,
          scanl
            ( \acc s ->
                acc V.// [(i, s)]
            )
            zwischenZahlV
            versionenZiff
            -- Der Anfangswert ist nicht benötigt
            & tail
            -- Die Änderungen sind in falscher Reihenfolge
            & reverse
        )

  -- Diese Funktion erhält einen Zwischenschritt `hexZahl` und erzeugt damit den
  -- nächsten Zwischenschritt. Die zusätzlichen Argumente `wegZg` und `hinZg`
  -- sind Zeiger auf die Zahl. Für sie gilt, dass links von ihnen jeweils
  -- schon alle Stäbchen die sich ändern müssen hingelegt bzw. weggelegt wurden.
  erzeugeZwStand :: (a ~ (Int, Int, Vector Segment)) => a -> a
  erzeugeZwStand (wegZg, hinZg, hexZahl) =
    -- `erstellenxZg` bestimmt den nächsten Index bei noch weg (bzw.) hingezogen
    -- werden muss. Weg wird mit `fst` als `sel` bestimmt, Hin wird mit `snd`
    -- als `sel` bestimmt. `xZg` ist jediglich zur Leistungsverbesserung da.
    -- Da links von den Zeigern `wegZg`/`hinZg` schon alle Stäbchen weg/hin gelegt
    -- wurden, muss in diesem Bereich nicht mehr gesucht werden.
    -- Dadurch dass von links nach rechts gesucht wird, erstellt `erzeugeZwStand`
    -- stets Zahlen ohne Löcher, solange die Startzahl länger als die Lösung ist.
    let erstellenxZg sel xZg =
          V.zip
            hexZahl
            lösungV
            -- Suche nicht links von `xZg`
            & V.drop xZg
            -- Suche den Index, bei welchem noch weg/hin gelegt werden muss.
            & V.findIndex ((/= 0) . sel . uncurry wegUndHinWechselSeg)
            -- Es muss stehts einen solchen Index geben, da `erzeugeZwStand`
            -- nicht auf die Lösung aufgerufen wird.
            & fromJust
            -- Der Index ist wegen dem `V.drop xZg` um xZg zu klein.
            & (+ xZg)
        nWegZg = erstellenxZg fst wegZg
        nHinZg = erstellenxZg snd hinZg
     in ( nWegZg,
          nHinZg,
          -- Hier wird beim Zwischstand `hexZahl` ein Stäbchen von Stelle
          -- `nWegZg` zu Stelle `nHinZg` getauscht.
          -- Dazu wird der Vektor `hexZahl` an diesen Stellen überschrieben.
          -- Sind die Stellen `nWegZg` und `nHinZg` können nie gleich sein.
          -- `a \\ b` wird hier benutzt, um die Stäbchen zu finden, welche in a,
          -- aber nicht in b liegen, also diejenigen die gewechselt werden müssen.
          -- `firstOne a` wird benutzt um eines der Stäbchen, die gewechselt werden
          -- müssen zu auszuwählen.
          -- `xor` wird benutzt um die ausgewählten Stäbchen je nachdem
          -- weg-/hinzulegen.
          hexZahl
            V.// [ ( nWegZg,
                     let w = hexZahl V.! nWegZg
                         h = lösungV V.! nWegZg
                      in w `xor` firstOne (w \\ h)
                   ),
                   ( nHinZg,
                     let w = hexZahl V.! nHinZg
                         h = lösungV V.! nHinZg
                      in w `xor` firstOne (h \\ w)
                   )
                 ]
        )

--- IO

-- | `parseHexZahl` ist ein Parser für Hexadezimalzahlen.
parseHexZahl :: Parsec Void String [Int]
parseHexZahl = some $ digitToInt <$> C.hexDigitChar

-- | `erstelleHexMax` liest ein HexMax-Objekt aus einer Datei heraus.
erstelleHexMax :: Bool -> FilePath -> IO HexMax
erstelleHexMax umlUnbegrenzt pfad = do
  inhalt <- readFile pfad

  either (die . errorBundlePretty) pure $
    let v = void
        parser = do
          hexZahl <- parseHexZahl
          v C.newline
          umleg' <- L.decimal
          let umleg = bool (Just umleg') Nothing umlUnbegrenzt
          pure $ HexMax{..}
     in parse @Void parser "" inhalt

-- | `genHexMax` erstellt ein zufälliges HexMax-Objekt
genHexMax :: Int -> Maybe Int -> IO HexMax
genHexMax hexZahlLänge umleg = do
  ersteZiffer <- randomRIO (1, 15)
  hexZahl <- (ersteZiffer :) <$> replicateM (hexZahlLänge - 1) (randomRIO (0, 15))
  pure $ HexMax{..}

-- | Die Hauptfunktion. Sie implementiert das UI.
main :: IO ()
main = execParser (O.info conf O.fullDesc) >>= runKonf
 where
  aarg = O.argument O.auto

  -- Das Programm hat 3 Unterbefehle, "datei", "zufall" und "direkt".
  conf =
    Konf
      <$> O.switch (O.long "no-log" <> O.short 'l')
      <*> O.switch (O.long "var-länge" <> O.short 'v')
      <*> O.switch (O.long "min" <> O.short 'm')
      <*> O.subparser
        ( O.command
            "datei"
            ( O.info
                ( UBDatei
                    <$> O.strArgument (O.metavar "DATEIPFAD")
                    <*> O.switch (O.long "umleg-unbegrenzt" <> O.short 'u')
                )
                O.idm
            )
            <> O.command
              "zufall"
              ( O.info
                  ( UBZufall
                      <$> aarg (O.metavar "INTEGER")
                      <*> optional (aarg (O.metavar "INTEGER"))
                  )
                  O.idm
              )
            <> O.command
              "direkt"
              ( O.info
                  ( UBDirekt
                      <$> ( HexMax
                              <$> O.argument
                                ( O.eitherReader $
                                    first errorBundlePretty . parse @Void (parseHexZahl <* eof) ""
                                )
                                (O.metavar "STRING")
                              <*> optional (aarg (O.metavar "INTEGER"))
                          )
                  )
                  O.idm
              )
        )

  runKonf (Konf{..}) =
    runHexMax noLog varLänge minHexZahl =<< case untBefehle of
      UBDirekt hexMax -> pure hexMax
      UBZufall l umleg -> do
        hexMax <- genHexMax l umleg
        putStrLn $ "Hexzahl: " <> zfZuChar (hexZahl hexMax)
        pure hexMax
      UBDatei datei umlegUnendlich -> erstelleHexMax umlegUnendlich datei

  zfZuChar = fmap (toUpper . intToDigit)

  -- `zeigeSegment` wandelt ein Segment in einen String um.
  zeigeSegment seg =
    maybe
      ( if seg == 0
          then " "
          else "[" <> showBin seg 7 <> "]"
      )
      (zfZuChar . (: []))
      (segmentZuZiffer seg)

  runHexMax noLog varLänge minHexZahl hexMax =
    let (züge, lösung) = löseHexMax minHexZahl varLänge hexMax
     in do
          -- FIXME
          -- putStrLn $ "Test: " <> show (sum (popCount . zifferZuSegment <$> hexZahl hexMax) == sum (popCount . zifferZuSegment <$> lösung))
          if noLog
            then do
              putStrLn $ "Lösung:  " <> zfZuChar lösung
              putStrLn $ show züge <> " Umlegungen"
            else do
              mapM_
                putStrLn
                [ show i <> ": " <> concat (zeigeSegment <$> ls)
                  | (i, ls) <- zip [0 :: Int ..] (erstelleUmlegungen züge (hexZahl hexMax) lösung)
                ]
