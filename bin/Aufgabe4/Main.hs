module Main (main) where

import Control.Monad (replicateM, void, when)
import Data.Bits (Bits (setBit), popCount, testBit, xor)
import Data.Bool (bool)
import Data.Foldable (find, foldl', traverse_)
import Data.Function ((&))
import Data.Functor ((<&>))
import Data.List (foldl1', partition, sort)
import Data.List.NonEmpty (NonEmpty ((:|)), nonEmpty)
import Data.Maybe (fromJust, listToMaybe)
import Data.Void (Void)
import System.Environment (getArgs)
import System.Exit (die)
import System.Random (randomRIO)
import System.Random.Shuffle (shuffleM)
import Text.Megaparsec
import qualified Text.Megaparsec.Char as C
import qualified Text.Megaparsec.Char.Lexer as L
import Text.Read (readMaybe)

--- Hilfsfunktionen

-- | Von ghc@Util
-- Die Funktion `f` wird `n`-mal mit sich selbst verknüpft.
-- `nTimes n f = (!!n) . iterate f`
nTimes :: Int -> (a -> a) -> (a -> a)
nTimes 0 _ = id
nTimes 1 f = f
nTimes n f = f . nTimes (n - 1) f

-- | Erstellt einen String mit den Ziffern der Zahl `n` in Binärdarstellung
-- bis Stelle `l`.
showBin :: Bits a => a -> Int -> String
showBin n l =
  reverse
    [ bool '0' '1' (testBit n i)
      | i <- [0 .. l - 1]
    ]

--- Typen

-- | Karten repräsentiert die Daten wie sie auch in den Beispieldateien gegeben sind.
data Karten = Karten
  { gesamtLänge :: Int,
    schlüsselAnzahl :: Int,
    bitWeite :: Int,
    codeWörter :: [Integer]
  }
  deriving stock (Show, Eq)

--- Lösung

-- | `indizesIntegerZuListe` bestimmt die Indizes aller Bits mit Wert 1.
indizesIntegerZuListe :: Bits a => Int -> a -> [Int]
indizesIntegerZuListe gesamtLänge n = filter (testBit n) [0 .. gesamtLänge - 1]

-- | `indizesListeZuInteger` setzt für alle `i ∈ is` in der zurrückgegebenen
-- Ganzzahl ein Bit an Stelle i auf 1.
indizesListeZuInteger :: [Int] -> Integer
indizesListeZuInteger is = foldl' setBit 0 is

-- | `bestimmeZyklen` findet mindestens `gesamtLänge - bitweite` Zyklen in `karten`.
-- Zyklen bezeichnet hier eine Menge an Zahlen, welche mit `xor` akkumuliert 0 ergibt.
bestimmeZyklen :: Int -> [Integer] -> [Integer]
bestimmeZyklen bitWeite codeWörter =
  -- `foldr` akkumuliert hier eine Liste von Tupeln. Dabei hält für die Bits
  -- der ersten Zahl im Tupel b₁, b₂, …, die Codewörter c₁, c₂, … und den Wert
  -- der zweiten Zahl x (⊕ stellt hier xor da):
  -- x = (b₁·c₁) ⊕ (b₂·c₂) ⊕ …
  -- Die erste Zahl im Tupel zeichnet also auf, aus welchen codeWörtern die Zweite
  -- besteht.
  let codeWörterMitIndex = zip [indizesListeZuInteger [i] | i <- [0 :: Int ..]] codeWörter
   in foldr
        ($)
        codeWörterMitIndex
        -- `go` wird für jedes Bit der codeWörter angewandt
        (go <$> [0 .. bitWeite - 1])
        -- Der erste Wert enthält die Indizes eines Zirkels, der zweite muss 0 sein.
        -- Daher wird der zweite Wert nicht mehr benötigt.
        & fmap fst
 where
  -- Nennen wir die Elemente in `cws` `a1`, deren zweite Ganzzahl das Bit 1 an
  -- Stelle `i` hat. Dann nimmt `go` das erste Element von `a1` und xort deren
  -- Ganzzahlen mit denen des Rests von `a1`. Die so entstandene Menge wird mit
  -- der Menge an Element welche in `cws`, aber nicht in `a1` sind, vereinigt.
  -- Offensichtlich hat die entstandene Menge ein Element weniger als `cws`.
  -- Komplexität: O(bitWeite·gesamtLänge²)
  go :: Int -> [(Integer, Integer)] -> [(Integer, Integer)]
  go i cws =
    -- `a1` enthält die Elemente deren zweite Ganzzahl das Bit 1 an Stelle `i` hat.
    -- `a0` enthält die Elemente mit Bit 0
    let (a1, a0) = partition ((`testBit` i) . snd) cws
     in case nonEmpty a1 of
          Just ((!cw', !ks') :| a1') ->
            -- `cw ⊕ cw'` hat an der Stelle `i` das Bit 0.
            -- `ks ⊕ ks'` stellt die Faktoren von `cw ⊕ cw'` dar.
            a0 <> (a1' <&> \(!cw, !ks) -> (cw `xor` cw', ks `xor` ks'))
          Nothing -> cws -- Falls a1 leer ist, kann direkt `cws` zurrückgegeben werden

-- | `findeSchlüssel` nimmt ein Kartenobjekt und gibt die Schlüsselwörter zusammen mit
-- dem Sicherheitswort nach Größe sortiert zurrück.
findeSchlüssel :: Karten -> [Integer]
findeSchlüssel (Karten{..}) =
  -- Der leere Zyklus wird hinzugefügt.
  let !zyklen = 0 : bestimmeZyklen bitWeite codeWörter
   in -- Die Menge `zyklen` wird mit sich selbst `schlüsselAnzahl+1` mal mit xor verknüpft.
      nTimes
        (schlüsselAnzahl - 1)
        (\acc -> concat $ acc <&> \a -> xor a <$> zyklen)
        zyklen
        -- Wenn die Daten richtig sind muss mindestens einen Zyklus mit Länge
        -- `schlüsselAnzahl+1` existieren
        & find (\ !x -> popCount x == schlüsselAnzahl + 1)
        & fromJust
        -- Die in der Ganzzahl gespeicherten Indizes werden in eine Liste umgewandelt
        & indizesIntegerZuListe gesamtLänge
        -- Die Indizes werden zurück zu ihren Codewörtern abgebildet.
        & fmap (codeWörter !!)
        -- Die Wörter sollen sortiert zurrück gegeben werden
        & sort

--- IO

-- | Die Funktion parst die Datei `pfad` in ein Objekt vom Typ `Karten`.
erstelleLochkarten :: String -> IO Karten
erstelleLochkarten pfad = do
  inhalt <- readFile pfad

  either (die . errorBundlePretty) pure $
    let v = void
        int = L.decimal
        integerB = L.binary
        parser = do
          gesamtLänge <- int
          C.space
          schlüsselAnzahl <- int
          C.space
          bitWeite <- int
          v C.newline
          codeWörter <- some do
            karte <- integerB
            v C.newline <|> eof
            pure karte
          pure $ Karten{..}
     in parse @Void parser "" inhalt

-- | Die Funktion erstellt ein zufälliges Objekt vom Typ `Karten` mit den übergebenden Eigenschaften
-- `bitWeite`, `schlüsselAnzahl` und `gesamtLänge`.
genLochkarten :: Int -> Int -> Int -> IO Karten
genLochkarten bitWeite schlüsselAnzahl gesamtLänge = do
  -- `wörter` generiert `n` zufällige Wörter
  let wörter n = replicateM n $ randomRIO (0 :: Integer, 2 ^ bitWeite - 1)

  schlüssel <- wörter schlüsselAnzahl
  let sicherheit = foldl1' xor schlüssel
  spam <- wörter $ gesamtLänge - schlüsselAnzahl - 1
  codeWörter <- shuffleM (sicherheit : schlüssel <> spam)

  pure $ Karten{..}

-- | `druckeErgebnis` druckt die Codewörter.
druckeCodeWörter :: Karten -> [Integer] -> IO ()
druckeCodeWörter (Karten{..}) = traverse_ (\n -> putStrLn $ showBin n bitWeite)

-- | Die Hauptfunktion. Sie implementiert das UI.
main :: IO ()
main = do
  args <- getArgs

  case nonEmpty args of
    Nothing -> die "Unterbefehl erwartet"
    Just ("datei" :| args') -> do
      pfad <- maybe (die "Dateipfad erwartet") pure (listToMaybe args')
      karten <- erstelleLochkarten pfad
      druckeCodeWörter karten $ findeSchlüssel karten
    Just ("zufall" :| args') ->
      case traverse readMaybe args' of
        Just ns@[bitWeite, schlüsselAnzahl, gesamtLänge] -> do
          when (any (<= 0) ns) $
            die "Argumente müssen *positive* Ganzzahlen sein"
          karten <- genLochkarten bitWeite schlüsselAnzahl gesamtLänge
          druckeCodeWörter karten $ findeSchlüssel karten
        Just _ -> die "Falsche Anzahl an Argumenten"
        Nothing -> die "Argumente müssen Ganzzahlen sein"
    Just _ -> die "Unbekannter Unterbefehl"
